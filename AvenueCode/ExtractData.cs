﻿using AvenueCode.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvenueCode
{
    public class ExtractData
    {
        private readonly int _FirstNode = 0;
        private readonly int _SecondNode = 1;
        private readonly int _Weigth = 2;

        public ExtractData()
        { }

        public Graph Run(string file)
        {
            if (!File.Exists(file))
                throw new ArgumentException("The file could not be read on path");

            string[] edges;

            using (var sr = new StreamReader(file))
            {
                string line = sr.ReadToEnd().Trim();
                if (String.IsNullOrEmpty(line))
                    throw new EmptyFileException("The file is empty!");

                edges = line.Split(',').Select(x => x.Trim()).ToArray();
            }

            if (!edges.Any())
                throw new InvalidFormatInputException("Input is an invalid format!");

            var nodes = getNodesFromInput(edges);
            var graph = new Graph(nodes);
            buildGraph(graph, edges);

            return graph;
        }

        private void buildGraph(Graph graph, string[] edges)
        {
            foreach(var edge in edges)
            {
                var charArrayEdge = edge.ToCharArray();
                int weigth = Int32.Parse(charArrayEdge[this._Weigth].ToString());
                graph.AddEdge(charArrayEdge[this._FirstNode], charArrayEdge[this._SecondNode], weigth);
            }
        }

        private List<char> getNodesFromInput(string[] edges)
        {
            var nodes = new List<char>();
            foreach (var edge in edges)
            {
                if (edge.Length != 3)
                    throw new InvalidFormatInputException("Any edge on input file is an invalid format");

                var charArrayEdge = edge.ToCharArray();
                for (int i = 0; i <= this._SecondNode; i++ )
                {
                    if (!nodes.Contains(charArrayEdge[i]))
                        nodes.Add(charArrayEdge[i]);
                }                  
            }
            return nodes.OrderBy(x => x).ToList();
        }
    }
}
