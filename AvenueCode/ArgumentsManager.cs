﻿using AvenueCode.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvenueCode
{
    public class ArgumentsManager
    {
        public void Run(string[] args)
        {
            try
            {
                Graph graph = new ExtractData().Run(args[0]);
                var arg1 = args[1].ToLower();
                switch (arg1)
                {
                    case "distance":
                        var distanceResult = graph.Distance(args[2]);
                        Output.PrintOutputDistance(distanceResult, args[2]);
                        break;
                    case "availableroutes":
                        var availableRoutesResult = graph.AvailableRoutes(args[2], args[3], Int32.Parse(args[4]));
                        Output.PrintOutputAvailableRoutes(availableRoutesResult);
                        break;
                    case "shortestroute":
                        var shortestRouteResult = graph.ShortestRoute(args[2], args[3]);
                        Output.PrintOutputShortestRoute(shortestRouteResult);
                        break;
                    default:
                        throw new ArgumentException("Program's type not found!");
                }
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (EmptyFileException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (NoSuchRouteException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
