﻿using AvenueCode.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvenueCode
{
    public static class Calculator
    {
        #region Distance
        /// <summary>
        /// Get the route's distance on graph
        /// </summary>
        /// <param name="graph"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        public static IDictionary<string, int> Distance(this Graph graph, string input)
        {
            var result = new Dictionary<string, int>();
            var nodes = input.Split('-').Select(x => x.Trim()).ToArray();

            if (nodes.Count() < 2)
                throw new InvalidFormatInputException("Argument invalid to define distance between routes.");

            int distance = 0;

            for (int i = 1; i < nodes.Count(); i++)
            {
                var previousNode = nodes[i - 1].ToCharArray().First();
                var currentNode = nodes[i].ToCharArray().First();

                if (graph.Matrix[graph.Nodes.IndexOf(previousNode), graph.Nodes.IndexOf(currentNode)].HasValue)
                    distance += graph.Matrix[graph.Nodes.IndexOf(previousNode), graph.Nodes.IndexOf(currentNode)].Value;
                else
                    throw new NoSuchRouteException("No such route to input!");
            }
            result.Add(input, distance);     
            return result;
        }
        #endregion

        #region Shortest Route
        /// <summary>
        /// Get the shortest route on graph
        /// </summary>
        /// <param name="graph"></param>
        /// <param name="firstNode"></param>
        /// <param name="lastNode"></param>
        /// <returns></returns>
        public static IDictionary<int, string> ShortestRoute(this Graph graph, string firstNode, string lastNode)
        {
            var visited = new LinkedList<string>();
            var routes = new Dictionary<int, string>();
            visited.AddFirst(firstNode);
            calculateShortestRoute(graph, visited, routes, lastNode);

            if (routes.Any())
            {
                int distance = routes.OrderBy(x => x.Key).Select(x => x.Key).FirstOrDefault();
                var shortestRoute = routes[distance];
            }
            return routes;
        }            

        /// <summary>
        /// Recursive method to calculates the shotest route
        /// </summary>
        /// <param name="graph"></param>
        /// <param name="visited"></param>
        /// <param name="routes"></param>
        /// <param name="lastNode"></param>
        private static void calculateShortestRoute(Graph graph, LinkedList<string> visited,  IDictionary<int, string> routes, string lastNode)
        {
            IDictionary<string, int> nodes = graph.GetAdjacentNodesWithWeigth(visited.Last.Value);
            foreach (var node in nodes)
            {
                if (visited.Contains(node.Key) && !node.Key.Equals(lastNode))
                    continue;

                if (node.Key.Equals(lastNode))
                {
                    visited.AddLast(node.Key);
                    int distance = graph.CalcTotalDistanceFromRoute(visited.ToArray()); 
                   
                    if (!routes.ContainsKey(distance + node.Value))
                        routes.Add(distance + node.Value, String.Join("-", visited));
                    else
                        routes[distance + node.Value] += "-" + visited;

                    visited.RemoveLast();
                    break;
                }
            }
            //Recursive
            foreach (var node in nodes)
            {
                if ((visited.Contains(node.Key) && !node.Key.Equals(lastNode)) || node.Key.Equals(lastNode))
                    continue;
                visited.AddLast(node.Key);
                calculateShortestRoute(graph, visited, routes, lastNode);
                visited.RemoveLast();
            }
        }
        #endregion

        #region Available Routes
        /// <summary>
        /// Get the available routes
        /// </summary>
        /// <param name="graph"></param>
        /// <param name="firstNode"></param>
        /// <param name="lastNode"></param>
        /// <param name="maximunStops"></param>
        /// <returns></returns>
        public static IList<string>AvailableRoutes(this Graph graph, string firstNode, string lastNode, int maximunStops)
        {
            var visited = new LinkedList<string>();
            var routes = new List<string>();
            visited.AddFirst(firstNode);
            getAllAvailableRoutes(graph, visited, routes, lastNode, maximunStops);
            return routes;
        }

        /// <summary>
        /// Recursive method to get all available routess
        /// </summary>
        /// <param name="graph"></param>
        /// <param name="visited"></param>
        /// <param name="routes"></param>
        /// <param name="lastNode"></param>
        /// <param name="maximunStops"></param>
        private static void getAllAvailableRoutes(Graph graph, LinkedList<string> visited, IList<string> routes, string lastNode, int maximunStops)
        {
            IDictionary<string, int> nodes = graph.GetAdjacentNodesWithWeigth(visited.Last.Value);
            foreach (var node in nodes)
            {
                if (visited.Count > maximunStops)
                    continue;

                if (node.Key.Equals(lastNode))
                {
                    visited.AddLast(node.Key);
                    int distance = graph.CalcTotalDistanceFromRoute(visited.ToArray());
                    routes.Add(String.Join("-", visited));
                    visited.RemoveLast();
                    break;
                }
            }
            //Recursive
            foreach (var node in nodes)
            {
                if (visited.Count > maximunStops)
                    continue;
                visited.AddLast(node.Key);
                getAllAvailableRoutes(graph, visited, routes, lastNode, maximunStops);
                visited.RemoveLast();
            }
        }
        #endregion
    }
}
