﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvenueCode.Tests
{
    [TestFixture]
    class TestRoutes
    {
        private readonly SetupGraph _Setup;
        public TestRoutes()
        {
            this._Setup = new SetupGraph();
        }

        [Test]
        public void AvailableRoutesTest()
        {
            //Arrange
            var graph = this._Setup.getGraphToTest();

            //Act
            var result = graph.AvailableRoutes("C", "C", 3);
            var expected = new List<string>();
            expected.Add("C-D-C");
            expected.Add("C-E-B-C");

            //Assert
            Assert.Contains(expected.First(), new Collection<string>(result));
            Assert.Contains(expected.Last(), new Collection<string>(result));
        }

        [Test]
        public void DistanceTest()
        {
            //Arrange
            var graph = this._Setup.getGraphToTest();

            //Act
            var result = graph.Distance("A-B-C");
            var expected = new Dictionary<string, int>();
            expected.Add("A-B-C", 9);

            //Assert
            Assert.AreEqual(expected.ContainsKey("A-B-C"), result.ContainsKey("A-B-C"));
            Assert.AreEqual(expected["A-B-C"], result["A-B-C"]);
        }

        [Test]
        public void ShortestRoutesTest()
        {
            //Arrange
            var graph = this._Setup.getGraphToTest();

            //Act
            var result = graph.ShortestRoute("A", "C");
            var expected = new Dictionary<int, string>();
            expected.Add(9, "A-B-C");            

            //Assert
            Assert.AreEqual(expected.ContainsKey(9), result.ContainsKey(9));
            Assert.AreEqual(expected[9], result[9]);
        }
    }
}
