﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvenueCode.Tests
{
    public class SetupGraph
    {
        public Graph getGraphToTest()
        {
            var nodes = getNodes();
            var graph = new Graph(nodes);
            graph.AddEdge('A', 'B', 5);
            graph.AddEdge('B', 'C', 4);
            graph.AddEdge('C', 'D', 8);
            graph.AddEdge('D', 'C', 8);
            graph.AddEdge('D', 'E', 6);
            graph.AddEdge('A', 'D', 5);
            graph.AddEdge('C', 'E', 2);
            graph.AddEdge('E', 'B', 3);
            graph.AddEdge('A', 'E', 7);

            return graph;
        }

        private IList<char> getNodes()
        {
            var nodes = new List<char>();
            nodes.Add('A');
            nodes.Add('B');
            nodes.Add('C');
            nodes.Add('D');
            nodes.Add('E');
            return nodes;
        }
    }
}
