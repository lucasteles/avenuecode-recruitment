﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using AvenueCode.Exceptions;

namespace AvenueCode
{

    /// <summary>
    /// Graph Model - Adjacency matrix
    /// 
    /// http://www2.dcc.ufmg.br/livros/algoritmos-edicao2/cap7/transp/completo4/cap7.pdf
    /// </summary>
    public class Graph
    {
        public int?[,] Matrix { get; set; }
        public IList<char> Nodes { get; set; }

        public Graph(IList<char> Nodes)
        {
            this.Matrix = new int?[Nodes.Count, Nodes.Count];
            this.Nodes = Nodes;
        }
        
        /// <summary>
        /// Add edge on the graph
        /// </summary>
        /// <param name="nodeA"></param>
        /// <param name="nodeB"></param>
        /// <param name="weigth"></param>
        public void AddEdge(char nodeA, char nodeB, int weigth)
        {
            this.Matrix[this.Nodes.IndexOf(nodeA), this.Nodes.IndexOf(nodeB)] = weigth;
        }

        /// <summary>
        /// Get the adjacent node and the respective edge's weigth
        /// </summary>
        /// <param name="last"></param>
        /// <returns></returns>
        public IDictionary<string, int> GetAdjacentNodesWithWeigth(string last)
        {
            var edgeWigth = new Dictionary<string, int>();
            for (int i = 0; i < this.Nodes.Count; i++)
            {
                if (this.Matrix[Nodes.IndexOf(last.ToCharArray().First()), i].HasValue)
                {
                    edgeWigth.Add(this.Nodes[i].ToString(), this.Matrix[Nodes.IndexOf(last.ToCharArray().First()), i].Value);
                }
            }
            return edgeWigth;
        }

        /// <summary>
        /// Calculates the edge's sum from set of nodes
        /// </summary>
        /// <param name="nodes"></param>
        /// <returns></returns>
        public int CalcTotalDistanceFromRoute(string[] nodes)
        {
            if (nodes.Count() <= 1)
                return 0;

            int distance = 0;
            for (int i = 1; i < nodes.Count() - 1; i++ )
            {
                var previousNode = Nodes.IndexOf(nodes[i - 1].ToCharArray().First());
                var nextNode = Nodes.IndexOf(nodes[i].ToCharArray().First());

                if (!this.Matrix[previousNode, nextNode].HasValue)
                    throw new EdgeValueNotFoundException("Edge value null or not found!");                        

                distance += this.Matrix[previousNode, nextNode].Value;
            }
            return distance;
        }
    }
}
