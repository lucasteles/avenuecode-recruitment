﻿using System;

namespace AvenueCode
{
    class Program
    {
        static void Main(string[] args)
        {
            var argManager = new ArgumentsManager();
            argManager.Run(args);
        }
    }
}
