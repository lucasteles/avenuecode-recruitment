﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvenueCode.Exceptions
{
    class EdgeValueNotFoundException : Exception
    {
        public EdgeValueNotFoundException()
        { }

        public EdgeValueNotFoundException(string message)
            : base(message)
        { }

        public EdgeValueNotFoundException(string message, Exception inner)
            : base(message, inner)
        { }
    }
}
