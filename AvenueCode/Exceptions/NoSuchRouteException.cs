﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvenueCode.Exceptions
{
    class NoSuchRouteException : Exception
    {
        public NoSuchRouteException()
        { }

        public NoSuchRouteException(string message)
            : base(message)
        { }

        public NoSuchRouteException(string message, Exception inner)
            : base(message, inner)
        { }
    }
}
