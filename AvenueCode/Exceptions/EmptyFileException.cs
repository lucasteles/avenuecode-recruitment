﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvenueCode.Exceptions
{
    public class EmptyFileException : Exception
    {
        public EmptyFileException()
        { }

        public EmptyFileException(string message)
            : base(message)
        { }

        public EmptyFileException(string message, Exception inner)
            : base(message, inner)
        { }
    }
}
