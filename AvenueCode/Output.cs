﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvenueCode
{
    /// <summary>
    /// Class responsible to show the specific result (from prompt command)
    /// </summary>
    public static class Output
    {
        //Show the available route result
        public static void PrintOutputAvailableRoutes(IList<string> routes)
        {
            foreach (var route in routes)
            {
                var amountStops = route.Split('-').Count();
                Console.WriteLine("{0} ({1} stops)", route, amountStops);
            }
        }

        //Show the distance result
        public static void PrintOutputDistance(IDictionary<string, int> route, string input)
        {
            Console.WriteLine("Input: {0} - {1}", input, route[input]);
        }

        //Show the shortest route result
        public static void PrintOutputShortestRoute(IDictionary<int, string> routes)
        {
            int distance = routes.OrderBy(x => x.Key).Select(x => x.Key).FirstOrDefault();
            var shortestRoute = routes[distance];

            if (!String.IsNullOrEmpty(shortestRoute))
                Console.WriteLine("{0} (distance = {1})", shortestRoute, distance);
            else
                Console.WriteLine("No route");
        }
    }
}
