Requirements
- .NET 4.5
- Visual Studio 2013 Ultimate

I wrote the code on two computers. The first has Windows 8.1 64bit and the last has Windows 8 64bit



Compile
The project was developed on Console Application. To run the project, I go to executable folder (the executable calls AvenueCode.exe) and run the command line below:

For Distance - AvenueCode.exe c:\graph.txt Distance A-B-C-D
For Available Routes - AvenueCode.exe c:\graph.txt AvailableRoutes A C 4
For Shortest Route - AvenueCode.exe c:\graph.txt ShortestRoute A C

All commands I assumed the input was on 'c:\graph.txt'.

Notice: I tried copy and past from instructions paper the example for distance 'A-B-C-D' and I saw the project run wrong. This wrong was the symbol '-'. This symbol is different of the hyphen, that I assumed. However, use the symbol hyphen from keyboard.



Input
This project consider the input is on the format below:

AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7

Each edge is separated for one comma and one space.



Tests
This project used NUNIT library for tests. However, to run the tests on Visual Studio 2013, I had to install another library called NUNIT Adapter.
To run the tests on Visual Studio, install NUNIT adapter and click on [TestFixture] selector and again click on 'Run Tests'.